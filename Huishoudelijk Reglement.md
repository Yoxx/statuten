# Huishoudelijk Reglement

Huishoudelijk Reglement van Studievereniging Syntax gevestigd te Leiden.

## Begripsbepalingen

### Artikel 1.
In dit Huishoudelijk Reglement wordt verstaan onder:

- *Vereniging:*
  
    Studievereniging Syntax, gevestigd te Leiden en ingeschreven bij de Kamer van Koophandel onder nummer 60302445 op 25 maart 2014.

- *Contributie:*
  
    De jaarlijks vastgestelde, financiële bijdrage die een lid aan de Vereniging betaalt;

- *Lidmaatschapsjaar:*
  
    Staat gelijk aan de periode van een studiejaar, van 1 september tot en met 31 augustus;

- *Statuten:*

    De statuten van de Vereniging zoals vastgelegd in een akte, gepasseerd op 12 december 2018 bij notariskantoor TeekensKarstens te Leiden;

- *Bestuur:*

    Het bestuur van de Vereniging, zoals gedefinieerd in artikel 1 van de Statuten;

- *Lid:*

    Een persoon zoals gedefinieerd in artikel 5 van de Statuten.


## Contributie

### Artikel 2.

1. De contributie voor een Lidmaatschapsjaar bedraagt €13,37. 
    Deze contributie is;
    - jaarlijks verschuldigd aan de Vereniging;
    - onafhankelijk van het moment van lid worden;
    - niet restitueerbaar.
2. Voor Alumni van Studievereniging Syntax geldt een gereduceerde contributie voor een Lidmaatschapsjaar van €5,00.


## Betaling contributiegelden

### Artikel 3.

1. De contributiegelden dienen betaald te worden wegens automatische incasso, het moment hiervoor wordt door de penningmeester bepaald.
2. Zolang het lid niet betaalt, blijft deze het contributiegeld schuldig aan de studievereniging.


## Beleidsregels omtrent gedrag

### Artikel 4.
Bij Studievereniging Syntax staat veiligheid en respect hoog in het vaandel. Zowel binnen als buiten de Vereniging worden de leden van Studievereniging Syntax geacht eenieder gelijk te bejegenen. Om deze reden zijn de volgende beleidsregels opgesteld.

1. Elke vorm van ongewenst gedrag, waarvan ieder weldenkend mens zou begrijpen dat deze zo ervaren kan worden.
    Ongewenst gedrag kan zijn:
    - direct of indirect seksueel getinte uitingen;
    - direct of indirect uitingen van intimidatie;
    - direct of indirect uitingen van discriminatie;
    - het toepassen van lichamelijk geweld.
2. Eenieder zorgt voor een omgeving en sfeer waarbinnen anderen zich veilig kunnen voelen.
3. Eenieder onthoudt zich ervan een ander te bejegenen op een wijze dat de persoon zich in zijn waardigheid voelt aangetast en verder in het privéleven van een ander door te dringen dan nodig is voor het gestelde doel van de Vereniging.


## Beleidsregels omtrent verdovende middelen

### Artikel 5.

  1. Studievereniging Syntax heeft een zero-tolerance beleid m.b.t. drugsgebruik door deelnemers tijdens, vlak voor of in de buurt van verenigingsactiviteiten.
  2. Uitzondering geschiedt op de in lid 1 bedoelde verdovende middelen wanneer dit medicijnen zijn met rechtmatige toepassing. 
  3. Een deelnemer wordt verwijderd van een activiteit door de organiserende commissie en het bestuur,
     wanneer er last ondervonden wordt van gebruik of invloed van:
     - (medicinale) drugs;
     - alcohol.


## Maatregelen bij schending

### Artikel 6.
Ter voorkoming en bestrijding van ongewenst gedrag zijn de volgende beleidsmaatregelen door het Bestuur vastgesteld.

1. Er wordt aandacht besteed aan de veiligheid, gezondheid en welzijn van de leden.
2. Voor hen die zich schuldig maken aan schending van het Huishoudelijk Reglement, de Statuten, ongewenst gedrag of imagoschade voor de Vereniging, worden passende maatregelen vastgesteld door het Bestuur.
    Maatregelen kunnen omvatten, maar zijn niet beperkend tot:
    - een waarschuwing;
    - een tijdelijke onthouding van alcoholische drank bij verenigingsactiviteiten;
    - een tijdelijke onthouding van deelname aan verenigingsactiviteiten;
    - een opzegging van het lidmaatschap door ontzegging of ontzetting uit de Vereniging.


## Vertrouwenspersoon

### Artikel 7.

1. Een vertrouwenspersoon dient als aanspreekpunt voor problemen en binnengekomen klacht(en) binnen de Vereniging en behandelt deze nauwkeurig. De vertrouwenspersoon kan slachtoffers van ongewenst gedrag van andere leden bijstaan.
2. Elk Lidmaatschapsjaar kan ieder lid zich aanmelden voor de positie vertrouwenspersoon, die vervolgens wordt verkozen op een ALV.
3. Als er niet genoeg aanmeldingen zijn of er plotseling afwezigheid van een vertrouwenspersoon is, wordt dit opgevangen door het bestuur. Een wijziging zal bekend gemaakt worden door het bestuur aan al haar leden.
4. Een vertrouwenspersoon kan zelf beslissen of een situatie geschikt is om als vertrouwenspersoon te behandelen, om vervolgens daar naar te handelen.
5. Wanneer een lid een melding maakt bij een vertrouwenspersoon zal deze vertrouwelijk worden behandeld.
6. Een vertrouwenspersoon kan bij een melding advies geven, de persoon doorverwijzen naar de juiste instantie en in overleg met partijen rond de tafel zitten, dit kan — indien voorkeur — zonder het desbetreffende lid.
7. Het bestuur kan een vertrouwenspersoon op non-actief zetten zoals de Statuten mogelijk maken. Daarna moet in uiterlijk drie weken een ALV worden georganiseerd, waarin het besluit van het bestuur wordt behandeld en een beslissing over de activiteit van de vertrouwenspersoon wordt genomen.


---

Mocht je een van de bovenstaande punten willen bespreken of een melding willen doen, dan kan dit door het Bestuur of de huidige vertrouwensperson(en) te contacteren.


