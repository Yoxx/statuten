# Statuten

Statuten van S.V. Syntax Leiden

## Begripsbepalingen

### Artikel 1.
In deze statuten wordt verstaan onder:

- *Algemene Ledenvergadering:*

    de algemene vergadering zoals bedoeld in de wet, zijnde het orgaan van de
    Vereniging dat wordt gevormd door stemgerechtigde leden van de Vereniging;

- *Alumni*

    Leden die reeds zijn afgestudeerd aan de opleiding Informatica aan Hogeschool Leiden;

- *Bestuur:*

    het Bestuur van de Vereniging;

- *Contributie*
    
    De jaarlijks vastgestelde, financiële bijdrage die een lid aan de
    Vereniging betaald;

- *Externe leden*

    Leden buiten de opleiding Informatica en/of ook buiten de
    Hogeschool Leiden;

- *Schriftelijk:*

    bij brief, fax of e-mail, of bij boodschap die via een ander gangbaar
    communicatiemiddel wordt overgebracht en elektronisch of op schrift kan
    worden ontvangen mits de identiteit van de verzender met afdoende zekerheid
    kan worden vastgesteld;

- *Statuten:*

    de statuten van de Vereniging;

- *Vereniging:*

    de rechtspersoon waarop de Statuten betrekking hebben.

## Naam, zetel en duur

### Artikel 2.

1. De Vereniging draagt de naam: **Studievereniging Syntax**.
2. De vereniging is gevestigd in de gemeente Leiden.

### Artikel 3.

1. Een verenigingsjaar loopt van een september tot en met eenendertig augustus van het opvolgende jaar.
2. Het bestuursjaar loopt gelijk aan het verenigingsjaar.
3. Het boekjaar loopt gelijk aan het verenigingsjaar.

## Doel van de Vereniging

### Artikel 4.

1. De Vereniging heeft ten doel het bevorderen, respectievelijk het doen bevorderen van:

    a. het contact tussen studenten onderling en tussen studenten en medewerkers van de opleiding Informatica aan Hogeschool Leiden;
    
    b. het organiseren van studiegerichte, dan wel ontspannende activiteiten;
    
    c. de kwaliteit van de opleiding zelve;
    
    d. de studiebelangen van alle studenten die staan ingeschreven bij de opleiding informatica aan de hogeschool Leiden;
    
    e. het vinden, respectievelijk aanbieden van stage- en carrière posities en het vergroten van betrokkenheid van de studenten in het werkveld.
    
2. De Vereniging tracht dit doel ondermeer te bereiken door:

    a. het bevorderen en organiseren van, of medewerking verlenen aan, activiteiten waarbij de studie respectievelijk het vakgebied centraal staat;
    
    b. het organiseren van, of medewerking verlenen aan, activiteiten welke ten doel hebben een band te scheppen tussen studenten van eerder genoemde opleiding;

    c. zorg te dragen voor de vertegenwoordiging van studenten binnen de eerder genoemde opleiding, daar waar het behartigen van studentbelangen gewenst kan zijn;
    
    d. het gebruik maken van alle wettige middelen die aan het doel van de Vereniging dienstbaar kunnen zijn.
    
## Leden

### Artikel 5.

1. Leden van de Vereniging zijn de natuurlijke personen die door het Bestuur als
lid zijn toegelaten. Daarbij wordt onderscheid gemaakt tussen:

    a. personen die ingeschreven staan bij de opleiding Informatica aan
    Hogeschool Leiden;
    
    b. Alumni;
    
    c. door de Algemene Ledenvergadering benoemde ere-leden;
    
    d. Externe Leden.

2.  Aanmeldingen voor het lidmaatschap dienen Schriftelijk te geschieden en is een inschrijving voor onbepaalde tijd.

3. De leden zijn gehouden tot het betalen van een Contributie, die door de eerste Algemene Ledenvergadering van het nieuwe verenigingsjaar 
wordt vastgesteld.

4. Het Bestuur is in bijzondere gevallen bevoegd het lid geheel of gedeeltelijk te ontheffen van de betalingsverplichting 
van de Contributie. Een dergelijk besluit behoeft drie/vierde meerderheid van stemmen in een bestuursvergadering,
waarin een meerderheid van het Bestuur aanwezig moet zijn.

5. Het Bestuur houdt een register bij waarin de namen en adresgegevens van
alle leden zijn opgenomen.

6. Het lidmaatschap is persoonlijk en niet overdraagbaar noch vatbaar om door
erfopvolging te worden verkregen.

7. In het huishoudelijk reglement kan een nadere omschrijving worden gegeven
van de verschillende vormen van leden.

8. Leden dienen zich te houden aan de Statuten en het huishoudelijk reglement.
Daarnaast zijn ook besluiten van het Bestuur en de algemene ledenvergadering leidend.

## Ereleden

### Artikel 6.

1. Ereleden van de Vereniging zijn zij, die als zodanig, op voordracht van het
Bestuur of tenminste vijf leden, door de Algemene Ledenvergadering zijn
benoemd wegens hun bijzondere verdiensten voor de Vereniging. Een
dergelijk besluit van de Algemene Ledenvergadering behoeft een meerderheid
van tweederde van de geldig uitgebrachte stemmen.

2. Ereleden betalen geen Contributie.

3. Op ereleden zijn dezelfde regels van toepassing als op andere leden.

### Artikel 7.

1. Alumni zijn zij die afgestudeerd zijn aan opleiding Informatica aan de
Hogeschool Leiden en lid zijn bij de Vereniging.

2. Alumni betalen een aangepast bedrag voor hun lidmaatschap.

## Begunstigers

### Artikel 8.

1. Begunstigers zijn zij, die zich bereid hebben verklaard een financiële bijdrage te leveren.

2. Begunstigers hebben geen andere rechten en/of verplichtingen dan die in het
begeleidende contract zijn toegekend of opgelegd.

3. De rechten en verplichtingen van een begunstiger kunnen te allen tijde
wederzijds door opzegging worden beëindigd, behoudens dat de jaarlijkse
bijdrage over het lopende verenigingsjaar voor het geheel verschuldigd blijft.
Opzegging door de Vereniging geschiedt door het Bestuur.

## Einde lidmaatschap

### Artikel 9.

1. Het lidmaatschap kan worden beëindigd:

    a. door Schriftelijke opzegging van het lid;
    
    b. door het overlijden van het lid;
    
    c. door opzegging namens de Vereniging;  
        Deze kan geschieden wanneer een lid heeft opgehouden aan
        de vereisten voor het lidmaatschap bij de Statuten gesteld te
        voldoen, wanneer hij zijn verplichtingen jegens de Vereniging
        niet nakomt, als ook wanneer redelijkerwijs van de Vereniging
        niet gevergd kan worden het lidmaatschap te laten voortduren;
    
    d. door ontzegging.  
        Deze kan alleen worden uitgesproken wanneer een
        lid in strijd met de Statuten, reglementen of besluiten der Vereniging
        handelt, of de Vereniging op onredelijke wijze benadeelt.


2. Opzegging van het lidmaatschap door het lid kan het gehele jaar geschieden
en moet vóór de eerste dag van het verenigingsjaar in het bezit zijn van de
secretaris indien het lid vóór het komende verenigingsjaar uitgeschreven wil
zijn.

3. Opzegging namens de Vereniging geschiedt door het Bestuur.

4. Opzegging van het lidmaatschap door het lid of door de Vereniging kan
slechts geschieden met inachtneming van een opzeggingstermijn van vier
weken. Echter kan het lidmaatschap onmiddellijk worden beëindigd indien van
de Vereniging of van het lid redelijkerwijs niet gevergd kan worden het
lidmaatschap te laten voortduren of wanneer gedragingen zijn waargenomen
die in strijd zijn met de gedragsregels die te vinden zijn in het huishoudelijk
reglement.

5. Indien er een opzegging plaatsvind waarbij niet wordt gehouden aan het
opzegtermijn van vier weken, zal het lidmaatschap eindigen op het vroegst
toelaatbare tijdstip, vier weken na de datum waarop de opzegging plaatsvond.

6. Ontzetting uit het lidmaatschap geschiedt door de algemene
ledenvergadering, dit gebeurt met tenminste tweederde van de stemmen van
de aanwezige geldige leden.

7. Opzegging van het lidmaatschap door ontzetting geschiedt Schriftelijk met
opgave van redenen. Binnen één maand na kennisgeving, kan een lid
bezwaar indienen tegen het besluit. Gedurende dit beroepstermijn is het lid
geschorst. Dit bezwaar zal op de eerstvolgende Algemene Ledenvergadering
worden behandeld.

8. Wanneer het lidmaatschap in de loop van een verenigingsjaar eindigt of door
opzegging, blijft de Contributie voor het geheel verschuldigd.

## Geldmiddelen

### Artikel 10.

De geldmiddelen van de Vereniging bestaan uit de Contributies van de leden, de
bijdragen van de begunstigers, uit eventuele verkrijgingen ingevolge erfstellingen,
legaten en schenkingen, uit entreegelden, kantine-opbrengsten, opbrengst van
subsidies, reclame- en/of sponsorgelden, en tenslotte uit eventuele toevallige baten
en uit door de Vereniging georganiseerde activiteiten.

## Bestuur

### Artikel 11.

1. Het Bestuur bestaat uit tenminste drie personen, die door de algemene
ledenvergadering uit de leden van de Vereniging worden benoemd.

2. Het Bestuur bevat minstens een voorzitter, secretaris en penningmeester.

3. De benoeming van bestuursleden geschiedt uit één of meer bindende
voordrachten, ofwel een kandidaatstelling. Tot het opmaken van een
voordracht zijn bevoegd zowel het Bestuur als tenminste tien leden.

4. Indien in een vacature slechts één kandidaat is gesteld, wordt deze als ge- of
herkozen beschouwd.

5. Aan elke voordracht kan het bindend karakter worden ontnomen door een
genomen besluit van de Algemene Ledenvergadering.

6. Als er geen voordracht is opgemaakt, dan is de Algemene Ledenvergadering
vrij in de keuze voor een voordracht van bestuursleden. Over deze voordracht
wordt bij de eerstvolgende Algemene Ledenvergadering gestemd.

7. Elke functie in de kandidaatstelling moet worden ingestemd door de
Algemene Ledenvergadering, met de meerderheid van de geldige stemmen.

8. Elk bestuurslid is verplicht een jaar na zijn benoeming af te treden, en wel op
de jaarvergadering. Aftredende bestuursleden zijn terstond opnieuw te
benoemen. Wie in een tussentijdse vacature is benoemd, neemt ten aanzien
van het aftreden de plaats van zijn voorganger in.

9. Het bestuurslidmaatschap eindigt voorts:

    a. door het eindigen van het lidmaatschap van de Vereniging;
    
    b. door te bedanken na het instemmen van het Bestuur in diezelfde
    Algemene Ledenvergadering;
    
    c. indien het bestuurslid onder curatele wordt gesteld.

10. De tekenbevoegden en zonodig diens plaatsvervangers kunnen met
onmiddellijke ingang door het Bestuur van de functie worden ontheven:

    a. bij faillissement, indien deze surséance van betaling aanvraagt of krijgt
    of bij toepassing van wettelijke schuldsanering;
    
    b. bij gebleken wanbeheer.

11. De Algemene Ledenvergadering kan een bestuurslid schorsen of ontslaan,
indien zij daartoe termen aanwezig acht. Voor een dergelijk besluit is een
meerderheid vereist van tenminste tweederde van de geldig uitgebrachte
stemmen.

12. De bestuurders zijn bevoegd te allen tijde zelf hun ontslag te nemen, mits dit
Schriftelijk geschiedt met een opzeggingstermijn van ten minste drie
maanden.

13. Bestuursposities zijn enkel voor leden die ingeschreven staan bij de opleiding
Informatica aan de Hogeschool Leiden.

## Taken, vertegenwoordiging en bevoegdheden Bestuur

### Artikel 12.

1. Het Bestuur is belast met het besturen van de Vereniging.

2. De Vereniging wordt vertegenwoordigd door het Bestuur. De
vertegenwoordigingsbevoegdheid komt mede toe aan de volgende leden van het Bestuur:

    a. hetzij de voorzitter en de secretaris tezamen;
    
    b. hetzij de voorzitter en de penningmeester tezamen;
    
    c. hetzij de secretaris en de penningmeester tezamen.


 De bestuursleden kunnen zich daarbij door een Schriftelijk gevolmachtigde 
 doen vertegenwoordigen.

3. Voor het beschikken over liquide middelen zijn de handtekeningen van de
tekenbevoegden van het Bestuur voldoende.

4. Het Bestuur kan worden bijgestaan door bijzondere commissies, werkgroepen
of functionarissen, door het Bestuur te benoemen.

5. Bij huishoudelijk reglement kunnen nadere regels aangaande taken,
taakverdeling, vergaderingen, notulering en besluitvorming van het Bestuur
worden gegeven.

6. In alle gevallen, waarin de Vereniging een tegenstrijdig belang heeft met één
of meer bestuursleden kan de Algemene Ledenvergadering één of meer
personen aanwijzen om de Vereniging te vertegenwoordigen.

## Jaarstukken en benoeming- en bevoegdheden kascommissie

### Artikel 13.

1. Het Bestuur brengt op een Algemene Ledenvergadering - de jaarvergadering -
binnen drie maanden na afloop van het verenigingsjaar, behoudens
verlenging van deze termijn door de Algemene Ledenvergadering, haar
jaarverslag uit. Dit jaarverslag gaat over het afgelopen bestuursjaar gevoerde
beheer en de daarbij horende financiële realisatie. Dit jaarverslag wordt na
goedkeuring van de voorzitter en de penningmeester ondertekend. Ontbreekt
de handtekening van één van beiden, dan wordt daarvan onder opgaaf van
redenen melding gemaakt en indien benodigd uitgesteld tot deze
ondertekening is voldaan.

2. De Algemene Ledenvergadering benoemt jaarlijks uit de leden een
kascommissie van tenminste twee personen, die geen deel mogen uitmaken
van het Bestuur. De commissie onderzoekt de rekening en verantwoording
van het Bestuur en brengt aan de jaarvergadering verslag van haar
bevindingen uit.

3. Wenst de kascommissie zich te laten bijstaan door een deskundige, dan kan
zij zich met dat verzoek tot het Bestuur of de Algemene Ledenvergadering
wenden.

4. Het Bestuur is verplicht aan de kascommissie en de in de hierboven
benoemde deskundige alle gewenste inlichtingen te verschaffen, desgewenst
de kas en de waarden van de Vereniging aan de commissie en/of aan de
deskundige te tonen en inzage van de boeken en bescheiden van de
Vereniging te geven.

5. Goedkeuring door de Algemene Ledenvergadering van het jaarverslag en de
rekening en verantwoording strekt het Bestuur tot décharge.

6. Indien de goedkeuring van de rekening en verantwoording wordt geweigerd,
kan de Algemene Ledenvergadering een nieuwe kascommissie benoemen,
bestaande uit tenminste drie leden, welke commissie een nieuw onderzoek
doet van de rekening en verantwoording. Deze nieuwe commissie heeft
dezelfde bevoegdheden als de eerder benoemde commissie. Binnen één
maand na de benoeming brengt de nieuwe commissie aan de algemene
ledenvergadering verslag uit van haar bevindingen. Wordt ook dan de
goedkeuring geweigerd, dan neemt de Algemene Ledenvergadering die
maatregelen, welke door haar in het belang van de Vereniging nodig worden
geacht.

## Algemene Vergadering

### Artikel 14.

1. Aan de Algemene Ledenvergadering komen in de Vereniging alle
bevoegdheden toe, die niet door de wet of Statuten aan het Bestuur zijn
opgedragen.

2. In de jaarvergadering komt onder meer aan de orde:

    a. de vaststelling van de jaarlijks door de leden verschuldigde Contributie;

    b. het jaarverslag van de secretaris;

    c. het jaarverslag en de rekening en verantwoording van het Bestuur;

    d. het verslag van de kascommissie;

    e. de benoeming van de kascommissie en de eventuele benoeming van
    bestuursleden;

3. Andere Algemene Ledenvergaderingen worden gehouden zo vaak het Bestuur
dit wenselijk vindt.

4. De voorzitter en de secretaris van het Bestuur of hun vervangers treden als
zodanig ook op in de Algemene Ledenvergaderingen.

5. Tijdens de algemene ledenvergadering worden notulen door de secretaris of
door een door de voorzitter aangewezen lid van de Vereniging gemaakt.

6. De algemene ledenvergadering kan al dan niet op voorstel van het Bestuur
besluiten dat van de algemene ledenvergadering een notarieel proces-verbaal
wordt opgemaakt.

## Toegang tot de Algemene Ledenvergadering


### Artikel 15.

1. Toegang tot de Algemene Ledenvergadering hebben alle soorten leden van de
Vereniging. Geen toegang hebben geschorste leden of leden die niet voldaan
hebben aan de betalingsverplichting omtrent Contributie. Bij de behandeling
van de schorsing heeft het geschorste lid de bevoegdheid voor dat
agendapunt de vergadering bij te wonen en daarover het woord te voeren.
Ten aanzien van de schorsing heeft het geschorste lid geen stemrecht.

2. Over toelating van andere dan de hiervoor genoemde personen beslist het
Bestuur.

## Stemrecht en stemmingen

### Artikel 16.

1. Begunstigers hebben geen stemrecht.

2. Ieder soort lid wat toegang heeft tot de Algemene Ledenvergadering, heeft één
stem.

3. Ten aanzien van minderjarige leden, die zestien jaar of ouder zijn, geldt dat zij
binnen de Vereniging geacht worden handelingsbekwaam te zijn, zodat zij
actief en passief stemrecht en alle rechten en verplichtingen hebben, die
meerderjarige leden ook hebben, tenzij uitdrukkelijk anders bepaald.

4. Een lid kan zijn stem door een Schriftelijk daartoe gemachtigd ander lid doen
uitbrengen, met dien verstande dat niemand voor meer dan één ander lid een
stem kan uitbrengen en de machtiging is voldaan op de daartoe bestemde
wijze. De secretaris is, indien door meerdere leden gemachtigd, wel bevoegd
om voor meerdere leden een stem uit te brengen. Mits de secretaris niet
aanwezig is, neemt de vice voorzitter deze taak waar.

5. Machtiging geschiedt door een ingevuld en ondertekend machtigingsformulier
in te dienen bij het Bestuur voor de Algemene Ledenvergadering. Deze wordt
door de secretaris gecontroleerd en vastgelegd in de notulen.

6. Een lid heeft geen stemrecht over zaken, die hem, zijn echtgenoot/partner of
één van zijn bloed- of aanverwanten in de rechte lijn betreffen. In het geval
dat een van de machtigingen ook de stelling hierboven betreft, worden deze
overgegeven naar of de secretaris of de vicevoorzitter.

## Besluitvorming

### Artikel 17.

1. Op een Algemene Ledenvergadering moeten het aantal leden met stemrecht
groter of gelijk zijn aan het aantal bestuursleden plus één om de algemene
ledenvergadering plaats te laten vinden. In dit aantal worden de
bestuursleden niet meegeteld.

2. Voorzover de wet of deze Statuten niet anders bepalen, worden alle besluiten
van de Algemene Ledenvergadering genomen met volstrekte meerderheid van
de uitgebrachte stemmen. Dit betekent: de helft plus één.

3. Alle stemmingen ter vergadering geschieden mondeling, tenzij de voorzitter
een Schriftelijke stemming gewenst acht of één der stemgerechtigden dit vóór
de stemming verlangt.

4. Ten aanzien van het voorzitterschap, notulering en stemmingen van- en in de
Algemene Ledenvergadering, kunnen bij huishoudelijke reglementen nadere
regels worden gegeven.

5. Het in een vergadering uitgesproken oordeel van de voorzitter omtrent de
uitslag van een stemming is bindend. Hetzelfde geldt voor de inhoud van een
genomen besluit, voor zover werd gestemd over een niet Schriftelijk
vastgesteld voorstel.

6. Wordt onmiddellijk na het uitspreken van een beslissing of oordeel van de
voorzitter de juistheid daarvan betwist, dan vindt een nieuwe stemming plaats,
indien de meerderheid der vergadering dit verlangt. Door een nieuwe
stemming vervallen de rechtsgevolgen van de oorspronkelijke stemming.

7. Een éénstemmig besluit van alle leden, ook al zijn dezen niet in vergadering
bijeen, heeft, mits met voorkennis van het Bestuur genomen, dezelfde kracht
als een besluit van de Algemene Ledenvergadering. Ten aanzien van ontslag
van bestuursleden, fusie met aan andere Vereniging, omzetting in een andere
rechtspersoon, statutenwijziging en ontbinding dient echter steeds een
vergadering te worden uitgeschreven, die moet voldoen een de door de wet of
deze Statuten gestelde vereisten.

8. Om bezwaar te maken tegen een besluit van het Bestuur of de algemene
ledenvergadering kan een motie worden ingediend ter behandeling van dit
punt bij de eerstvolgende Algemene Ledenvergadering. Een motie kan enkel
worden aangedragen door tenminste tien leden en moet voor het
bekendmaken van de definitieve agenda binnen zijn bij het Bestuur, is dit niet
het geval zal de motie worden meegenomen bij de daarop volgende
vergadering, tenzij het Bestuur anders beslist of de meerderheid van de
Algemene Ledenvergadering hierop aandringt.

## Bijeenroeping Algemene Ledenvergadering

### Artikel 18.

1. De Algemene Ledenvergaderingen worden bijeengeroepen door het Bestuur.
De oproeping geschiedt hetzij Schriftelijk aan de adressen of e-mailadressen
van de leden volgens het ledenregister. De termijn voor oproeping bedraagt
tenminste twee weken.

2. Daarnaast is het Bestuur op Schriftelijk verzoek van tenminste een zodanig
aantal leden als bevoegd is tot het uitbrengen van één/tiende gedeelte van de
stemmen verplicht tot het bijeenroepen van een Algemene Ledenvergadering
op een termijn van niet langer dan vier weken. Indien aan het verzoek binnen
veertien dagen geen gevolg wordt gegeven, kunnen de verzoekers zelf tot die
bijeenroeping overgaan op de wijze waarop het Bestuur de algemene
ledenvergadering bijeenroept.

3. De leden worden op een termijn van tenminste één week in kennis en inzage
gesteld van de in de vergadering te behandelen onderwerpen.

## Statutenwijziging

### Artikel 19.

1. In de Statuten van de Vereniging kan geen verandering worden gebracht dan
door een besluit van een Algemene Ledenvergadering, waartoe is opgeroepen
met de mededeling dat aldaar wijziging van de Statuten zal worden
voorgesteld.

2. Zij die de oproeping tot de Algemene Ledenvergadering ter behandeling van
een voorstel tot statutenwijziging hebben gedaan, zullen deze wijzigingen
meesturen met de definitieve agenda.

3. Tot wijziging van de Statuten kan slechts worden besloten door een Algemene
Ledenvergadering, waar tenminste tweederde van het totaal aantal leden van
de Vereniging aanwezig of vertegenwoordigd is, met een meerderheid van
tenminste tweederde van het aantal uitgebrachte stemmen. Indien in die
vergadering het vereiste quorum niet aanwezig is, zal een nieuwe vergadering
worden uitgeschreven, te houden uiterlijk dertig dagen na de betrokken
vergadering, waarin ongeacht het aantal aanwezige leden besluiten genomen
kunnen worden met tenminste tweederde meerderheid.

4. Een statutenwijziging treedt niet in werking voordat hiervan een notariële akte
is opgemaakt. Tot het doen verlijden van de akte is ieder bestuurslid bevoegd.

5. De leden van het Bestuur zijn verplicht een authentiek afschrift van de
wijziging, alsmede de gewijzigde Statuten neer te leggen ten kantore van het
Handelsregister, gehouden door de Kamer van Koophandel.

## Ontbinding

### Artikel 20.

1. De Vereniging kan worden ontbonden door een besluit van de Algemene
Ledenvergadering. Tot ontbinding kan slechts worden besloten wanneer tenminste
tweederde van het totaal aantal leden van de Vereniging aanwezig of
vertegenwoordigd is, met een meerderheid van tenminste tweederde van het
aantal uitgebrachte stemmen. Indien in die vergadering het vereiste quorum
niet aanwezig is, zal een nieuwe vergadering worden uitgeschreven, te
houden uiterlijk dertig dagen na de betrokken vergadering, waarin ongeacht
het aantal aanwezige leden besluiten genomen kunnen worden met tenminste
tweederde meerderheid.

2. Het batig saldo na vereffening wordt ter beschikking gesteld van een door de
Algemene Ledenvergadering bij het besluit tot ontbinding aan te wijzen doel,
dat zoveel mogelijk met het doel van de Vereniging in overeenstemming is.

3. De vereffening geschiedt door de daartoe door de Algemene
Ledenvergadering, aangewezen persoon en bij gebreke van een dergelijke
aanwijzing door het Bestuur van de Vereniging.

4. De vereffenaars dragen er zorg voor dat de inschrijvingen van de Vereniging
worden ontbonden.

5. Na afloop van de vereffening blijven de boeken en andere door de wet
geaccepteerde gegevensdragers van de Vereniging gedurende tien jaar
berusten onder de jongste vereffenaar.

## Huishoudelijk Reglement

### Artikel 21.

1. De Algemene Ledenvergadering kan bij huishoudelijk reglement nadere regels
geven omtrent verscheidene zaken.

2. Het huishoudelijk reglement wordt vastgesteld en gewijzigd door de Algemene
Ledenvergadering, dit gebeurt met een meerderheid van de stemmen.

3. Wijziging van het huishoudelijke reglement kan geschieden bij besluit van de
Algemene Ledenvergadering indien dit Schriftelijk wordt verzocht door
tenminste tien leden of op voorstel van het Bestuur.

4. Het huishoudelijk reglement zal geen bepalingen mogen bevatten die afwijken
van of die in strijd zijn met de bepalingen van de wet of van de Statuten, tenzij
de afwijking door de wet of de Statuten wordt toegestaan.

5. In het na huishoudelijk reglement kan een nadere omschrijving
worden gegeven van de soorten leden.
